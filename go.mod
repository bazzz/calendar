module gitlab.com/bazzz/calendar

go 1.22.0

require (
	github.com/ahmetb/go-linq/v3 v3.2.0
	github.com/arran4/golang-ical v0.2.6
)
