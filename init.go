package calendar

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	ical "github.com/arran4/golang-ical"
)

// ISO8601DateTime represents a date + time in format 'yyyy-MM-dd HH:mm:ss' by returning Go format '2006-01-02 15:04:05'.
const ISO8601DateTime string = "2006-01-02 15:04:05"

func NewFromOnline(url string, username string, password string) (*Calendar, error) {
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, fmt.Errorf("could not create http request with url %v: %w", url, err)
	}
	request.SetBasicAuth(username, password)
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, fmt.Errorf("could not process http request for url %v: %w", url, err)
	}
	defer response.Body.Close()
	return new(response.Body)
}

func NewFromFile(path string) (*Calendar, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("could not open file %v: %w", path, err)
	}
	return new(file)
}

func new(reader io.Reader) (*Calendar, error) {
	cal, err := ical.ParseCalendar(reader)
	if err != nil {
		return nil, fmt.Errorf("could not parse ics calendar: %w", err)
	}
	return &Calendar{data: cal}, nil
}

// expandRepeatingEvent changes a repeating event into multiple seperate Events. If the provided event does not repeat is it returned as the only event.
func expandRepeatingEvent(event Event, endDate time.Time) []Event {
	events := make([]Event, 0)
	if event.RepeatRule == "" || !strings.HasPrefix(event.RepeatRule, "FREQ=") {
		events = append(events, event)
		return events
	}

	freq := ""
	var count int
	var interval int
	var until time.Time
	components := strings.Split(event.RepeatRule, ";")
	for _, component := range components {
		prefix := "FREQ="
		if strings.HasPrefix(component, prefix) {
			freq = component[len(prefix):]
			continue
		}
		prefix = "COUNT="
		if strings.HasPrefix(component, prefix) {
			countInt, err := strconv.Atoi(component[len(prefix):])
			if err != nil {
				panic(err)
			}
			count = countInt
			continue
		}
		prefix = "INTERVAL="
		if strings.HasPrefix(component, prefix) {
			intervalInt, err := strconv.Atoi(component[len(prefix):])
			if err != nil {
				panic(err)
			}
			interval = intervalInt
			continue
		}
		prefix = "UNTIL="
		if strings.HasPrefix(component, prefix) {
			until = parseTime(component[len(prefix):], time.Local)
			continue
		}
		prefix = "BYMONTHDAY="
		if strings.HasPrefix(component, prefix) {
			// No need to do anything. The day is already specified by Start.
			continue
		}
		prefix = "BYDAY="
		if strings.HasPrefix(component, prefix) && strings.Contains(event.RepeatRule, "FREQ=WEEKLY") {
			// No need to do anything. The day is irrelevant if freq is weekly because it will always be 7 days and therefore the same day.
			continue
		}
		log.Println("Unknown RRULE component:", component, "in", event.RepeatRule)
	}

	// if until is not set or endDate is earlier than until, then endDate is the until.
	if until.Year() == 1 || endDate.Sub(until) < 0 {
		until = endDate
	}

	switch freq {
	case "DAILY":
		days := 1
		if interval > 0 {
			days *= interval
		}
		clones := generateClones(event, until, count, 0, 0, days)
		events = append(events, clones...)
	case "WEEKLY":
		days := 7
		if interval > 0 {
			days *= interval
		}
		clones := generateClones(event, until, count, 0, 0, days)
		events = append(events, clones...)
	case "MONTHLY":
		months := 1
		if interval > 0 {
			months *= interval
		}
		clones := generateClones(event, until, count, 0, months, 0)
		events = append(events, clones...)
	case "YEARLY":
		years := 1
		if interval > 0 {
			years *= interval
		}
		clones := generateClones(event, until, count, years, 0, 0)
		events = append(events, clones...)
	}
	return events
}

func generateClones(event Event, until time.Time, count int, years int, months int, days int) []Event {
	clones := make([]Event, 0)
	start := event.Start
	end := event.End
	addCount := 0

	for until.Sub(start) > 0 && (count == 0 || addCount < count) {
		// Do not make a clone for a date that is in excluded dates.
		if dateSliceContains(event.ExcludedDates, start) {
			start = start.AddDate(years, months, days)
			end = end.AddDate(years, months, days)
			continue
		}
		clone := Event{
			Organizer:   event.Organizer,
			Attendee:    event.Attendee,
			Description: event.Description,
			Categories:  event.Categories,
			Class:       event.Class,
			Summary:     event.Summary,
			Start:       start,
			End:         end,
			Location:    event.Location,
			Status:      event.Status,
			FreeBusy:    event.FreeBusy,
			URL:         event.URL,
		}
		clones = append(clones, clone)
		start = start.AddDate(years, months, days)
		end = end.AddDate(years, months, days)
		addCount++
	}
	return clones
}

func dateSliceContains(slice []time.Time, value time.Time) bool {
	for _, content := range slice {
		if content.Year() == value.Year() && content.Month() == value.Month() && content.Day() == value.Day() {
			return true
		}
	}
	return false
}
