package calendar

import (
	"time"

	linq "github.com/ahmetb/go-linq/v3"
	ical "github.com/arran4/golang-ical"
)

type Calendar struct {
	data *ical.Calendar
}

// GetEvents returns all events having their starting moment between startDate and endDate.
func (c *Calendar) GetEvents(startDate time.Time, endDate time.Time) []Event {
	events := make([]Event, 0)
	for _, calEvent := range c.data.Events() {
		event := newEvent(calEvent)
		expandedEvents := expandRepeatingEvent(event, endDate)
		events = append(events, expandedEvents...)
	}

	// Filter out events that are not between the requested dates.
	filteredEvents := make([]Event, 0)
	for _, event := range events {
		if startDate.Sub(event.Start) < 0 && endDate.Sub(event.End) > 0 {
			filteredEvents = append(filteredEvents, event)
		}
	}
	linq.From(filteredEvents).
		OrderByT(func(x Event) string { return x.Start.Format(ISO8601DateTime) }).
		ToSlice(&filteredEvents)
	return filteredEvents
}
